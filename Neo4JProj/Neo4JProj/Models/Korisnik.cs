﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Neo4JProj.Models
{
    public class Korisnik
    {
        public String Idkorisnika { get; set; }

        public String Ime { get; set; }
        public String Prezime { get; set; }
        public String Email { get; set; }

        //korisnik da moze biti prijatelj nekog drugog korisnika (koji idu u istu plesnu skolu)
        public List<Korisnik> prijatelji { get; set; }
      
    }
}

