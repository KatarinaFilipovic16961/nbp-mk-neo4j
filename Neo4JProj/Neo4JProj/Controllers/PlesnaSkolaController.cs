﻿using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using Neo4JProj.Models;

namespace Neo4JProj.Controllers
{
    public class PlesnaSkolaController : Controller
    {
        private GraphClient client;

        public PlesnaSkolaController( )
        {
            //this.client = client;

            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "katarina");
            try
            {

                client.Connect();

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
         public IActionResult PrikaziPlesneSkole()
        {
            var query = new Neo4jClient.Cypher.CypherQuery(" match (n:plesnaskola) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<PlesnaSkola> skole = ((IRawGraphClient)client).ExecuteGetCypherResults<PlesnaSkola>(query).ToList();
            return View(skole);


        }
        public IActionResult dodajPlesnuSkolu()
        {
            return View();
        }

        public IActionResult sacuvajPlesnuSkoluUbazu(string Idskole, string Ime, string Grad, string Adresa, string Email, string Brojtel,string Ocena)
        {

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("idskole", Idskole);
            queryDict.Add("ime", Ime);
            queryDict.Add("grad", Grad);
            queryDict.Add("adresa", Adresa);
            queryDict.Add("email", Email);
            queryDict.Add("brojtel", Brojtel);
            queryDict.Add("ocena", Ocena);




            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:plesnaskola {idskole:'" + Idskole + "', ime:'" + Ime
                                                             + "', grad:'" + Grad + "', adresa:'" + Adresa
                                                             + "', email:'" + Email
                                                              + "', brojtel:'" + Brojtel
                                                               + "', ocena:'" + Ocena
                                                             + "'}) return n",queryDict, CypherResultMode.Set);

             List<PlesnaSkola> skole = ((IRawGraphClient)client).ExecuteGetCypherResults<PlesnaSkola>(query).ToList();

            return RedirectToAction("dodajPlesnuSkolu"); //ovde ide na prikazi sve
        }

        public IActionResult idiNaobrisiStranicu()//na ovoj ima za editovanje i brisanje
        {
            return View();
        }


        public IActionResult ObrisiPlesnuSkolu(string Idskole)//vodi racuna kako se zove parametar ako se prebaci negde
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("idskole", Idskole);
         
          
            var query = new Neo4jClient.Cypher.CypherQuery(" match (n:plesnaskola {idskole: '" + Idskole + "'}) detach delete n",
                                                             queryDict, CypherResultMode.Projection);
            

           

            PlesnaSkola skol = ((IRawGraphClient)client).ExecuteGetCypherResults<PlesnaSkola>(query).FirstOrDefault();
            return RedirectToAction("idiNaobrisiStranicu"); 

        }
        public IActionResult PromeniPlesnuSkolu(string staroIme,string novoIme)//edit ime skole
                                 //  start n = node(*) where(n:plesnaskola) and exists(n.ime) and n.ime='ChaTa' set n.ime= "mnogo" return n
        {

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("staroime", staroIme);
            queryDict.Add("novoime", novoIme);


            var query = new Neo4jClient.Cypher.CypherQuery(" start n = node(*) where(n:plesnaskola) and exists(n.ime) and n.ime='"+ staroIme+"' set n.ime= '"+novoIme+"' return n",
                                                             queryDict, CypherResultMode.Set);
            



            List<PlesnaSkola> skole = ((IRawGraphClient)client).ExecuteGetCypherResults<PlesnaSkola>(query).ToList();

            return RedirectToAction("idiNaobrisiStranicu");

        }

        public IActionResult OceniSkolu(string imeSkole,string ocena)//edit ime skole
        {

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("imeskole", imeSkole);
            queryDict.Add("ocena", ocena);

            float a = float.Parse(ocena);
            if(a>10 || a<0)
                return RedirectToAction("idiNaobrisiStranicu");


            var query1 = new Neo4jClient.Cypher.CypherQuery(" start n = node(*) where(n:plesnaskola) and exists(n.ime) and n.ime='" + imeSkole + "'  return n.ocena",
                                                             queryDict, CypherResultMode.Set);
            string ocen = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query1).FirstOrDefault();

            a = (a + float.Parse(ocen))/2;

            var query = new Neo4jClient.Cypher.CypherQuery(" start n = node(*) where(n:plesnaskola) and exists(n.ime) and n.ime='" + imeSkole + "' set n.ocena= '" + a.ToString() + "' return n",
                                                             queryDict, CypherResultMode.Set);




            List<PlesnaSkola> skole = ((IRawGraphClient)client).ExecuteGetCypherResults<PlesnaSkola>(query).ToList();

            return RedirectToAction("idiNaobrisiStranicu");


        }
        //    "MATCH (m:plesnaskola)" + "RETURN m ORDER BY ocena DESC LIMIT 10"


            //preporuke za najbolje ocenjjne  skole
        public IActionResult Preporuke()
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();



            var query = new CypherQuery("MATCH (m:plesnaskola)" +
                                        "RETURN m ORDER BY toFloat(m.ocena) DESC LIMIT 5",
                                        new Dictionary<string, object>(), CypherResultMode.Set);

            List<PlesnaSkola> skole = ((IRawGraphClient)client).ExecuteGetCypherResults<PlesnaSkola>(query).ToList();
            return View(skole);


        }


        //---------------------------------------VEZE------------------------------------------
        //PLESNA SKOLA IMA PLES

        //match (i:plesnaskola),(m:ples) where i.ime='MusicOn' and m.idplesa = '11' CREATE(i) -[r: IMA]->(m) --RADIIIIII

        public IActionResult dodajPlesSkoli(string idplesa, string ime)
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("ime", ime);
            queryDict.Add("idplesa", idplesa);


            //hocemo da dodamo ples ciji se id unosi u skolu cije se ime unos
            var query = new Neo4jClient.Cypher.CypherQuery("match (i:plesnaskola),(m:ples) where i.ime=' " + ime + "' and m.idplesa = '" + idplesa + "' CREATE(i) -[r: IMA]->(m)",
                                                            queryDict, CypherResultMode.Set);



            ((IRawGraphClient)client).ExecuteCypher(query);

            return RedirectToAction("idiNaDodavanjePlesa");
        }

        public IActionResult idiNaDodavanjePlesa()
        {
            return View();
        }




    }
}
